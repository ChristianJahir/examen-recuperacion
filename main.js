var arbol = new Arbol();

//Rama 1
arbol.nodoPadre.hI = arbol.agregarNodo(arbol.nodoPadre,"Hoja Izquierda","B");
arbol.nodoPadre.hD = arbol.agregarNodo(arbol.nodoPadre,"Hoja Derecha","C");
arbol.nodoPadre.hI.hI = arbol.agregarNodo(arbol.nodoPadre.hI,"Hoja Izquierda","D");
arbol.nodoPadre.hI.hI.hI = arbol.agregarNodo(arbol.nodoPadre.hI.hI,"Hoja Izquierda","G");
arbol.nodoPadre.hI.hI.hI.hD = arbol.agregarNodo(arbol.nodoPadre.hI.hI.hI,"Hoja Derecha","K");

//Rama 2
arbol.nodoPadre.hD.hI = arbol.agregarNodo(arbol.nodoPadre.hD,"Hoja Izquierda","E");
arbol.nodoPadre.hD.hI.hI = arbol.agregarNodo(arbol.nodoPadre.hD.hI,"Hoja Izquierda","H");
arbol.nodoPadre.hD.hI.hI.hI = arbol.agregarNodo(arbol.nodoPadre.hD.hI.hI,"Hoja Izquierda","L");
arbol.nodoPadre.hD.hI.hI.hI.hI = arbol.agregarNodo(arbol.nodoPadre.hD.hI.hI.hI,"Hoja Izquierda","M");
arbol.nodoPadre.hD.hI.hI.hI.hD = arbol.agregarNodo(arbol.nodoPadre.hD.hI.hI.hI,"Hoja Derecha","N");
arbol.nodoPadre.hD.hI.hI.hI.hD.hD = arbol.agregarNodo(arbol.nodoPadre.hD.hI.hI.hI.hD,"Hoja Derecha","O");
arbol.nodoPadre.hD.hI.hI.hI.hD.hD.hD = arbol.agregarNodo(arbol.nodoPadre.hD.hI.hI.hI.hD.hD,"Hoja Derecha","P");

//Rama 3
arbol.nodoPadre.hD.hD = arbol.agregarNodo(arbol.nodoPadre.hD,"Hoja Derecha","F");
arbol.nodoPadre.hD.hD.hI = arbol.agregarNodo(arbol.nodoPadre.hD.hD,"Hoja Izquierda","I");
arbol.nodoPadre.hD.hD.hD = arbol.agregarNodo(arbol.nodoPadre.hD.hD,"Hoja Derecha","J");

console.log("Nivel buscado: " + arbol.nivel);
var buscar=arbol.verificarNivelHijos(arbol.nodoPadre);
console.log(buscar);
var busqueda = "P";
console.log("Nodo A Buscar");
console.log (busqueda);
var busqueda = arbol.buscarValor(busqueda,arbol.nodoPadre);
var caminoNodo=arbol.buscarCaminoNodo(busqueda);
console.log("Camino recorrido:")
console.log(caminoNodo);
var sumaDeCaminos=arbol.sumarCaminoNodo(busqueda);
console.log("Suma del recorrido:");
console.log(sumaDeCaminos);
console.log("Arbol: ")
console.log(arbol);